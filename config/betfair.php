<?php

return [
    'appKey' => env('BETFAIR_APPKEY', ''),
    'username' => env('BETFAIR_USERNAME', ''),
    'password' => env('BETFAIR_PASSWORD', ''),
    'endpoint' => env('BETFAIR_ENDPOINT', 'https://identitysso.betfair.com/api/login'),
];
