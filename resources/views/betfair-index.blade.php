@extends('layouts.app')

@section('content')
    <h2>Methods</h2>

    <p><a href="{{ route('betfair.listeventtypes') }}">listEventTypes</a></p>
    <p><a href="{{ route('betfair.listcountries') }}">listCountries</a></p>
    <p><a href="{{ route('betfair.listmarketcatalogue') }}">listMarketCatalogue</a></p>
    <p><a href="{{ route('betfair.listevents') }}">listEvents</a></p>
    <p><a href="{{ route('betfair.listhorseracingevents') }}">listEvents (AU Horse Racing only)</a></p>
    <p><a href="{{ route('betfair.runnerbook') }}">listRunnerBook</a></p>
    <p><a href="{{ route('betfair.listcurrentorders') }}">listCurrentOrders</a></p>
    <p><a href="{{ route('betfair.listclearedorders') }}">listClearedOrders</a></p>
    <p><a href="{{ route('betfair.placeorders') }}">placeOrders</a></p>
@endsection
