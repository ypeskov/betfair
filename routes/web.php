<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'BetfairController@index')->name('betfair.index');

Route::get('/listevents', 'BetfairController@listEvents')->name('betfair.listevents');
Route::get('/listhorseracingevents', 'BetfairController@listHorseRacingEvents')->name('betfair.listhorseracingevents');
Route::get('/listeventtypes', 'BetfairController@listEventTypes')->name('betfair.listeventtypes');
Route::get('/listcountries', 'BetfairController@listCountries')->name('betfair.listcountries');
Route::get('/listmarketcatalogue', 'BetfairController@listMarketCatalogue')->name('betfair.listmarketcatalogue');
Route::get('/listrunnerbook', 'BetfairController@listRunnerBook')->name('betfair.runnerbook');
Route::get('/listcurrentorders', 'BetfairController@listCurrentOrders')->name('betfair.listcurrentorders');
Route::get('/listclearedorders', 'BetfairController@listClearedOrders')->name('betfair.listclearedorders');
Route::get('/placeorders', 'BetfairController@placeOrders')->name('betfair.placeorders');

Auth::routes();
