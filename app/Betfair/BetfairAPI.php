<?php
namespace App\Betfair;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Log;
use stdClass;

class BetfairAPI {
    /**
     * @var string application key from BetFair
     */
    private $appKey;

    /**
     * @var string
     */
    private $sessionToken;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    public function __construct() {
        $this->appKey = config('betfair.appKey');
        $this->username = config('betfair.username');
        $this->password = config('betfair.password');

        // try to login into API and get a session token
        $this->login();
    }

    private function login() {
        try {
            Log::info("Trying to login to Betfair API...");
            $loginResponse = $this->loginCall();
            if (mb_strlen($loginResponse->token) > 0) {
                $this->sessionToken = $loginResponse->token;
                Log::info("Betfair API login successful.");
            } else {
                Log::error('Error login in Betfair');
                throw new Exception($loginResponse);
            }

        } catch(Exception $e) {
//            ddd(json_decode($e->getMessage()));
            Log::error("Error logging in Betfair API: " . $e->getMessage());
        }
    }

    /**
     * @param string $command
     * @param string $params
     * @param int $numberOfAttempts
     * @return mixed
     * @throws Exception
     */
    public function command(string $command, string $params, int $numberOfAttempts=2) {
        $response = [];

        for($i=0; $i<$numberOfAttempts; $i++) {
            $attempt = $i + 1;
            Log::info(__METHOD__.' '.__LINE__.": Attempt #{$attempt}. Calling Betfair API [$command] with parameters: " . $params);

            try {
                $response = $this->betFairApiRequest($command, $params);

                Log::info("Betfair API response: " . json_encode($response));

                //no more attempts is required
                break;
            } catch(Exception $e) {
                $errorJson = json_decode($e->getMessage());

                Log::error(__METHOD__.' '.__LINE__.": Error calling Betfair API: " . $e->getMessage());

                if (isset($errorJson->detail->APINGException) &&
                    isset($errorJson->detail->APINGException->errorCode) &&
                    $errorJson->detail->APINGException->errorCode === 'INVALID_SESSION_INFORMATION') {

                    Log::info(__METHOD__.' '.__LINE__.": Trying to get fresh session token");
                    $this->login();

                    continue;
                }

                if (isset($errorJson->faultstring) && $errorJson->faultstring === 'DSC-0008') {
                    Log::error(__METHOD__.' '.__LINE__.': JSONDeserialisationParseFailure');

                    throw new Exception($e->getMessage());
                }
            }
        }

        return $response;
    }

    /**
     * @param string $from
     * @param string $to
     * @param int $numberOfAttempts
     * @return array
     * @throws Exception
     */
    public function listHorseRacingEventsAU(string $from, string $to, int $numberOfAttempts=2) {
        $fromDate = new Carbon($from);
        $toDate = new Carbon($to);

        $filterParams = new stdClass();
        $filterParams->marketCountries = ['AU',];

        // API date filtering doesn't work for some reason
        // So filtering will be done in the loop after getting data
//        $filterParams->marketStartTime = [
//            'from' => $from,
//            'to' => $to,
//        ];

        $params = json_encode(['filter' => $filterParams, ]);

        //get all available event types
        $eventTypes = $this->betFairApiRequest('listEventTypes', $params);

        //and finally get event type ID for "Horse Racing"
        $horseRacingTypeId = $this->getEventTypeIdByName('Horse Racing', $eventTypes);

        //now set up filter to get events for AU only
        $filterParams = new stdClass();
        $filterParams->marketCountries = ['AU',];
        $filterParams->eventTypeIds = [$horseRacingTypeId,];
        $params = json_encode(['filter' => $filterParams, ]);

        Log::info("Calling Betfair [listEvents] with parameters " . $params);

        $events = $this->command('listEvents', $params, $numberOfAttempts);

        $filteredEvents = [];
        foreach($events as $event) {
            $eventDate = new Carbon($event->event->openDate);
            if ($eventDate->greaterThanOrEqualTo($fromDate) && $eventDate->lessThanOrEqualTo($toDate)) {
                $filteredEvents[] = $event;
            }
        }

        return $filteredEvents;
    }

    /**
     * @param string $eventName
     * @param array $allEventTypes
     * @return int
     */
    private function getEventTypeIdByName(string $eventName, array $allEventTypes) {
        foreach ($allEventTypes as $eventType) {
            if ($eventType->eventType->name == 'Horse Racing') {
                return $eventType->eventType->id;
            }
        }

        return -1;
    }

    /**
     * @param string $operation
     * @param string $params
     * @return mixed
     * @throws Exception
     */
    private function betFairApiRequest(string $operation, string $params) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.betfair.com/rest/v1/$operation/");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Expect:',
            'X-Application: ' . $this->appKey,
            'X-Authentication: ' . $this->sessionToken,
            'Accept: application/json',
            'Content-Type: application/json'
        ));

        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

        $response = json_decode(curl_exec($ch));

        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($http_status == 200) {
//            ddd($response);
            return $response;
        } else {
//            ddd($response);
            throw new Exception(json_encode($response));
        }
    }

    /**
     * @return stdClass
     * @throws Exception
     */
    private function loginCall() {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, config('betfair.endpoint'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'X-Application: ' . $this->appKey,
            'Accept: application/json',
            'Content-Type: application/x-www-form-urlencoded'
        ]);

        $params = 'username='.$this->username.'&password='.$this->password;

        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

        $response = json_decode(curl_exec($ch));

        $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($http_status === 200) {
            if ($response->status !== 'FAIL') {
                return $response;
            } else {
                throw new Exception(json_encode($response));
            }
        } else {
//            ddd($response);
            throw new Exception(json_encode($response));
        }
    }
}
