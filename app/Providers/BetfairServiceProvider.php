<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Betfair\BetfairAPI;

class BetfairServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register() {
        $this->app->singleton(BetfairAPI::class, function ($app) {
            return new BetfairAPI();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
