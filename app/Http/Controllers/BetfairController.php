<?php

namespace App\Http\Controllers;

use App\Betfair\BetfairAPI;

class BetfairController extends Controller {
    /**
     * @var string
     */
    private $api;

    public function __construct(BetfairAPI $api) {
        $this->api = $api;
    }

    public function index() {
        return view('betfair-index');
    }

    public function listEvents() {
        $filterParams = new \stdClass();
        $filterParams->marketCountries = ['AU',];

        $params = json_encode([
            'filter' => $filterParams,
        ]);

        try {
            ddd($this->api->command('listEvents', $params));
        } catch(\Exception $e) {
            ddd($e->getMessage());
        }
    }

    public function listCurrentOrders() {
        try {
            ddd($this->api->command('listCurrentOrders', ''));
        } catch(\Exception $e) {
            ddd($e->getMessage());
        }
    }

    public function listClearedOrders() {
        $params = json_encode([
            'betStatus' => 'SETTLED',
        ]);
        try {
            ddd($this->api->command('listClearedOrders', $params));
        } catch(\Exception $e) {
            ddd($e->getMessage());
        }
    }

    public function listHorseRacingEvents() {
        $horseRacings = [];

        try {
            $horseRacings = $this->api->listHorseRacingEventsAU('2020-02-22T00:00:00Z', '2020-03-22T00:00:00Z');
        } catch(\Exception $e) {
            ddd($e->getMessage());
        }

        foreach($horseRacings as $racing) {
            dump($racing->event);
        }

        ddd(1);
    }

    public function placeOrders() {
        $limitOrder = new \stdClass();
        $limitOrder->size = '1';
        $limitOrder->price = '1000';
        $limitOrder->persistenceType = 'LAPSE';

        $instruction = new \stdClass();
        $instruction->selectionId = 29694499;
        $instruction->handicap = '0';
        $instruction->side = 'BACK';
        $instruction->orderType = 'LIMIT';
        $instruction->limitOrder = $limitOrder;

        $placeInstruction = new \stdClass();
        $placeInstruction->marketId = '1.169249113';

        $placeInstruction->instructions = [$instruction,];
        $placeInstruction->customerRef = 'fsdf';

        $params = json_encode($placeInstruction);
//        ddd($params);

        try {
            ddd($this->api->command('placeOrders', $params));
        } catch(\Exception $e) {
            ddd($e->getMessage());
        }
    }

    public function listEventTypes() {
        $filterParams = new \stdClass();
        $filterParams->marketCountries = ['AU',];

        $params = json_encode([
            'filter' => $filterParams,
        ]);

        try {
            $eventTypes = $this->api->command('listEventTypes', $params);

            foreach($eventTypes as $eventType) {
                dump($eventType->eventType);
            }

            ddd(sizeof($eventTypes));
        } catch(\Exception $e) {
            ddd($e->getMessage());
        }
    }

    public function listCountries() {
        $filterParams = new \stdClass();

        $params = json_encode([
            'filter' => $filterParams,
        ]);

        try {
            $countries = $this->api->command('listCountries', $params);
            ddd($countries);
        } catch(\Exception $e) {
            ddd($e->getMessage());
        }
    }

    public function listMarketCatalogue() {
        $filterParams = new \stdClass();
        $filterParams->marketCountries = ['AU',];
        $filterParams->eventTypeIds = ['7', ];

        $params = json_encode([
            'filter' => $filterParams,
            'maxResults' => 1000,
        ]);

        try {
            ddd($this->api->command('listMarketCatalogue', $params));
        } catch(\Exception $e) {
            ddd($e->getMessage());
        }
    }

    public function listRunnerBook() {
        $params = json_encode([
            'marketId' => '1.164681723',
            'selectionId' => 29567548,
        ]);

        try {
            ddd($this->api->command('listRunnerBook', $params));
        } catch(\Exception $e) {
            ddd($e->getMessage());
        }
    }
}
